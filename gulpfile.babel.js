import gulp from "gulp";

const config = {
	distDir: "./dist/",
	gulpDir: "./gulp/",
	assetsBuild: "./src/",
	errorLog: function(error) {
		console.log("\x1b[31m%s\x1b[0m", error);
		if (this.emit) {
			this.emit("end");
		}
	}
};

import { task as taskReact } from "./gulp/task-react";

export const react = () => taskReact(config);

export const watch = () => {
	const settings = { usePolling: true, interval: 100 };
	gulp.watch(config.assetsBuild + "**/*.{js,jsx}", settings, gulp.series(react));
};

export const taskDefault = gulp.series(gulp.parallel(react), watch);

export default taskDefault;
