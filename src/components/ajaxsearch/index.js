import React from 'react';

export default class Autocomplete extends React.Component {
	constructor(props) {
		super();
		this.state = { isLoaded: false, term: '', posts: [] };
		this.baseState = this.state;
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleEscape = this.handleEscape.bind(this);
		this.onChange = this.onChange.bind(this);
		this.resetForm = this.resetForm.bind(this);
	}

	componentDidMount() {
		window.addEventListener('keyup', this.handleEscape);
	}

	handleEscape(event) {
		if (event.key === 'Escape') {
			this.resetForm();
		}
	}

	getPosts() {
		const { term } = this.state;

		if (!term.length) {
			this.setState({ posts: [] });
			return;
		}

		this.setState({
			isLoaded: false
		});

		fetch(
			this.props.sourceUrl +
				'?_unique=' +
				Math.floor(Math.random() * 1000000) +
				'&s=' +
				term
		)
			.then(res => res.json())
			.then(
				result => {
					this.setState({
						isLoaded: true,
						posts: result
					});
					this.setState({ isLoaded: true });
				},
				error => {
					this.setState({
						isLoaded: true,
						error
					});
				}
			);
	}

	handleSubmit(event) {
		event.preventDefault();
		this.getPosts();
	}

	onChange(event) {
		this.setState({ term: event.target.value });
	}

	resetForm() {
		this.setState(this.baseState);
	}

	render() {
		const { posts, term, isLoaded } = this.state;

		return (
			<>
				<form onReset={this.resetForm} onSubmit={this.handleSubmit}>
					<input type="text" value={term} onChange={this.onChange} />
					{!!term.length && <input type="reset" value="&times;" />}
				</form>
				{!!posts.length && (
					<ul>
						{posts.map(item => (
							<li key={item.id}>
								{!!item.image && (
									<div
										dangerouslySetInnerHTML={{
											__html: `<a href="${item.link}">${item.image}</a>`
										}}
									></div>
								)}
								<a href={item.link}>
									<span
										dangerouslySetInnerHTML={{
											__html: item.title
										}}
									></span>
								</a>
								{item.date}
							</li>
						))}
					</ul>
				)}
				{!!isLoaded && !!term.length && !posts.length && (
					<p>No results</p>
				)}
			</>
		);
	}
}
