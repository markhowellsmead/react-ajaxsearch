import React from 'react';
import ReactDOM from 'react-dom';
import Autocomplete from './components/ajaxsearch';
const domContainer = document.querySelector("[data-id='ajaxsearch']");
const sourceUrl = domContainer.getAttribute('data-source');
ReactDOM.render(<Autocomplete sourceUrl={sourceUrl} />, domContainer);
