const filter = require('gulp-filter');

import gulp from 'gulp';
import webpack from 'webpack';
import gulpWebpack from 'webpack-stream';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';

export const task = config => {
	return (
		gulp
			.src([`${config.assetsBuild}/index.js`])
			.pipe(
				gulpWebpack(
					{
						mode: 'production',
						module: {
							rules: [
								{
									test: /\.(js|jsx)$/,
									exclude: /(node_modules)/,
									loader: 'babel-loader'
								}
							]
						},
						watchOptions: {
							poll: true,
							ignored: /node_modules/
						},
						output: {
							filename: 'dist.js'
						}
					},
					webpack
				)
			)
			.on('error', config.errorLog)
			.pipe(gulp.dest(config.distDir))

			// Minify
			.pipe(filter(['**/*.js']))
			.pipe(uglify())
			.pipe(
				rename({
					suffix: '.min'
				})
			)
			.on('error', config.errorLog)
			.pipe(gulp.dest(config.distDir))
	);
};
